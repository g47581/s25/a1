// #2


db.fruits.aggregate ([
			{
				$match: {"onSale": true}},
				{$count:"fruitsOnSale"}

			])

// #3
db.fruits.aggregate ([
			{$match: {"stock":{$gt:20}}},
			{$count:"enoughStock"}

])


// #4

db.fruits.aggregate ([

	{$match: {"onSale": true}},
	{$group:{_id: "null",
           avg_price: { $avg: "$price" }}

    }
])





// #5

db.fruits.aggregate (

	[
		{$match: {"onSale": true}},
		{
			$group: 
				{ 
					_id: "$name", 
					max_price: { $max: "$price" }
			}
		}
	]
)

// #6


db.fruits.aggregate (

	[
		{$match: {"onSale": true}},
		{
			$group: 
				{ 
					_id: "$name", 
					min_price: { $min: "$price" }
			}
		}
	]
)


